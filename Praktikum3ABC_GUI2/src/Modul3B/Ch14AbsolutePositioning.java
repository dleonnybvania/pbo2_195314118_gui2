/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul3B;

import java.awt.Color;
import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

public class Ch14AbsolutePositioning extends JFrame {
    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HEIGHT = 200;
    private static final int FRAME_X_ORIGIN = 300;
    private static final int FRAME_Y_ORIGIN = 100;
    private static final int BUTTON_WIDTH = 80;
    private static final int BUTTON_HEIGHT = 40;           
    private JButton cancelButton;
    private JButton okButton;
    private JTextField txtField;
    
    public static void main(String[] args) {
        Ch14AbsolutePositioning frame = new Ch14AbsolutePositioning();
        frame.setVisible(true);
    }
    
    public Ch14AbsolutePositioning(){
        Container contentPane = getContentPane();
        
        //set the frame properties
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setResizable(true);
        setTitle("Program Ch14AbsolutePositioning");
        setLocation(FRAME_X_ORIGIN, FRAME_Y_ORIGIN);
        
        //set the content pane properties
        contentPane.setLayout(null);
        contentPane.setBackground(Color.white);
        
        cancelButton = new JButton("CANCEL");
        cancelButton.setBounds(150, 105, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(cancelButton);
        
        //create and place two buttons on the frame's content pane
        okButton = new JButton("OK");
        okButton.setBounds(90, 125, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(okButton);
        
        
        
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    
}

