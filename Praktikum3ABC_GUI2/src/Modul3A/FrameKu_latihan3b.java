/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul3A;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class FrameKu_latihan3b extends JFrame {
    public FrameKu_latihan3b(){
        this.setSize(300, 500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Ini Class turunan dari class JFrame");
        this.setVisible(true);
        
        JPanel panel = new JPanel();
        JButton tombol = new JButton();
        tombol.setText("Ini Tombol");
        panel.add(tombol);
        
        
        JLabel label = new JLabel();
        label.setText("Ini LABEL");
        panel.add(label);
        
        JTextField textField = new JTextField();
        textField.setText("Ini TEXTFIELD");
        panel.add(textField);
        
        JCheckBox checkBox = new JCheckBox();
        checkBox.setText("Ini CHECKBOX");
        panel.add(checkBox);
        
        JRadioButton radioButton = new JRadioButton();
        radioButton.setText("Ini RADIOBUTTON");
        panel.add(radioButton);
        
        this.add(panel);
        
        
    }
    public static void main(String[] args) {
        new FrameKu_latihan3b();
    }
    
}
